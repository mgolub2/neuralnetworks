/**
 * Created by mgolub2 on 12/1/16.
 */
public class DataDump {

    public float [][] stateData;
    public float [] valueData;

    public DataDump(float [][] stateData, float [] valueData) {
        this.stateData = new float[stateData.length][stateData[0].length];
        for(int i = 0; i < stateData.length; i++) {
            this.stateData[i] = stateData[i].clone();
        }
        this.valueData = valueData.clone();
    }

}
