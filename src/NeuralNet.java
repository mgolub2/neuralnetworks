import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYSeries;

/**
 * @author mgolub2
 * An implementation of a neural network using the provided NeuralNetInterface
 */
public class NeuralNet implements NeuralNetInterface {

    private  int CUTOFF = 500; //Cutoff for epochs - nonconvergence
    //Arguments to the constructor
    private int numInputs;
    private int numHidden;
    private float lowerBound;
    private float upperBound;
    private String testName;
    private float errorThreshold = 0.07f;
    private float alpha;
    private float momentum;
    public enum ActivationType {
        BINARY, BIPOLAR
    }
    private ActivationType sigmoidType;
    private boolean graph;
    //class globals TODO: (can probably be moved into functions later)
    private int epoch;
    private float [][] inputsToHiddenWeights;
    private float [] hiddenToOutputWeights;
    private float [] hiddenOutputValues;
    private float [] hiddenToOutputWeightsM1;
    private float [][] inputsToHiddenWeightsM1;
    private float [] hiddenBiasWeight;
    private float outputBiasWeight;
    private float [] hiddenBiasWeightM1;
    private float outputBiasWeightM1;
    private float [] trainScale;

	/*
	 * Constructor.
	 * @param argNumInputs The number of inputs in your input vector
	 * @param argNumHidden The number of hidden neurons in your hidden layer. Only a single hidden layer is supported 
	 * @param argLearningRate The learning rate coefficient
	 * @param argMomentumTerm The momentum coefficient
	 * @param argA Integer lower bound of sigmoid used by the output neuron only.
	 * @param arbB Integer upper bound of sigmoid used by the output neuron only.
	 */
	public NeuralNet ( int argNumInputs, int argNumHidden, float argLearningRate, float argMomentumTerm,
                       float argA, float argB, ActivationType sigmoid,
                       boolean load, boolean graph, float [] trainScale) {
        numInputs = argNumInputs;
        numHidden = argNumHidden;
        lowerBound = argA; //maybe remove these
        upperBound = argB;
        sigmoidType = sigmoid;
        alpha = argLearningRate;
        momentum = argMomentumTerm;
        this.trainScale = trainScale;
        this.graph = graph;
        //scale = (float) ((upperBound-lowerBound)/2.0);
        //hacky but whatever
        switch (sigmoidType) {
            case BINARY:
                testName = "Binary with momentum:" + momentum + ", learning rate: " + argLearningRate;
                break;
            case BIPOLAR:
                testName = "Bipolar with momentum:" + momentum + ", learning rate: " + argLearningRate;
                break;
        }
        initializeWeights();
        rebuild();
        if(load){
            try {
                initializeWeights();
                load("nn_8.txt");
            }
            catch (IOException e) {
                System.out.println("Unable to load file!");
                initializeWeights();
            }
        }
	}

    private void rebuild() {
        hiddenOutputValues = new float[numHidden];
        epoch = 0;
        hiddenToOutputWeightsM1 = new float[numHidden];
        inputsToHiddenWeightsM1 = new float[numHidden][numInputs];
        hiddenBiasWeightM1 = new float[numHidden];
        outputBiasWeightM1 = 0;
    }

    /*
    Run the neural network until the error reaches below a threshold E
     */
	public int run(float [][] inputVectors, float [] expectedVector) { // execVec will also someday be a float array
        //do some iterations over the input vectors
        XYSeries errorPerEpoch = new XYSeries("Error per Epoch");
        float E = 1.0f; //set intial E above thresh
        float lastE = 0;
        while (E > errorThreshold && Math.abs(lastE-E) > 0.0001) {
            System.out.println(epoch);
            System.out.println(E);
            lastE = E;
            if (epoch == CUTOFF) {
                //save("nn_8.txt");
                E = Float.POSITIVE_INFINITY;
                break;
                //rebuild();
                //return 0;
            }
            epoch += 1;
            float epochError = 0;
            for(int pattern = 0; pattern < inputVectors.length; pattern++) {
                epochError += Math.pow(expectedVector[pattern]-train(inputVectors[pattern], expectedVector[pattern]), 2);
            }
            E = (float) Math.sqrt(epochError/(float) inputVectors.length);
            errorPerEpoch.add(epoch, E);
        }
        int oldEpoch = epoch;
        if (graph) {
            outputGraph(errorPerEpoch);
        }
        save("nn_8.txt");
        rebuild();
        return oldEpoch;
    }

    private void outputGraph(XYSeries data) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(data);
        JFreeChart chart = ChartFactory.createXYLineChart(
                testName, // chart title
                "Epoch", // axis label
                "Error", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                false, // include legend
                true, // tooltips
                false // urls
        );
        try {
            ChartUtilities.saveChartAsJPEG(new File("g"+testName+".jpg"), chart, 640, 480);
        }
        catch (IOException e) {
            System.out.println("Unable to save error graph.");
        }
    }

    public void run(float [][] inputVectors, float [] expectedVector, float argErrorThreshold) {
        errorThreshold = argErrorThreshold;
        this.run(inputVectors, expectedVector);
    }
	
	/* (non-Javadoc)
	 * @see CommonInterface#outputFor(float[])
	 */
	@Override
	public float outputFor(float[] X) {
	    for(int i = 0; i < trainScale.length; i++) {
	        X[i] = X[i]/trainScale[i];
        }
        //System.out.println(Arrays.toString(X));
		for (int neuron = 0; neuron < numHidden; neuron++) {
            hiddenOutputValues[neuron] = activationFunction(weightedSumBias(X, inputsToHiddenWeights[neuron], hiddenBiasWeight[neuron]));
        }
        //calculate the bias output
        return activationFunction(weightedSumBias(hiddenOutputValues, hiddenToOutputWeights, outputBiasWeight));
	}

	private float weightedSumBias(float [] inputs, float [] weights, float biasWeight) {
		float sum = biasWeight;
		for (int i = 0; i < inputs.length; i++) {
            sum += inputs[i]*weights[i];
		}
		return sum;
    }

	/* (non-Javadoc)
	 * @see CommonInterface#train(float[], float)
	 */
	@Override
	public float train(float[] X, float argValue) {
        float output = outputFor(X);
        float tempOutputBiasWeight = outputBiasWeight;
        float [] tempHiddenToOutputWeights = hiddenToOutputWeights.clone();
        float [] tempHiddenBiasWeights = hiddenBiasWeight.clone();
        float  [][] tempInputsToHiddenWeights = new float[numHidden][numInputs];
        //for (int j = 0; j < numHidden; j++) {
        //    tempInputsToHiddenWeights[j] = inputsToHiddenWeights[j].clone();
        //}
        //calc the hidden to output weight deltas
        float errorPortionK = (argValue-output)*sigmoidDx(output);
        for (int j = 0; j < numHidden; j++) {
            tempInputsToHiddenWeights[j] = inputsToHiddenWeights[j].clone();
            float outputDelta = alpha*errorPortionK*hiddenOutputValues[j];
            //merge with above later
            hiddenToOutputWeights[j] = hiddenToOutputWeights[j]+outputDelta+(hiddenToOutputWeights[j]- hiddenToOutputWeightsM1[j])*momentum;
            hiddenToOutputWeightsM1[j] = tempHiddenToOutputWeights[j];
            //calc the input to hidden delta
            //gamma_k*alpha*w_jk*dx(z_in)
            float inputToHiddenErrorPortion = alpha * errorPortionK * hiddenToOutputWeights[j] * sigmoidDx(hiddenOutputValues[j]);
            hiddenBiasWeight[j] = hiddenBiasWeight[j] + inputToHiddenErrorPortion + (hiddenBiasWeight[j]- hiddenBiasWeightM1[j])*momentum;
            hiddenBiasWeightM1[j] = tempHiddenBiasWeights[j];
            //calculate the input deltas and update the weights
            for (int x = 0; x < numInputs; x++) {
                inputsToHiddenWeights[j][x] = inputsToHiddenWeights[j][x]+inputToHiddenErrorPortion*X[x]
                        + (inputsToHiddenWeights[j][x]- inputsToHiddenWeightsM1[j][x])*momentum;
                inputsToHiddenWeightsM1[j][x] = tempInputsToHiddenWeights[j][x];
            }
        }
        float outputBiasDelta = alpha*errorPortionK;
        //update the output bias
        outputBiasWeight = outputBiasWeight + outputBiasDelta + (outputBiasWeight - outputBiasWeightM1) * momentum;
        outputBiasWeightM1 = tempOutputBiasWeight;
        return output;
	}

	/* (non-Javadoc)
	 * @see CommonInterface#save(java.io.File)
	 */
	@Override
	public void save(String argFile) {
        try{
            PrintWriter writer = new PrintWriter(argFile, "UTF-8");
            writer.println(numInputs);
            writer.println(numHidden);
            for(int i = 0; i < inputsToHiddenWeights.length; i++) {
                writer.println(Arrays.toString(inputsToHiddenWeights[i]).replace("[", "").replace("]", "")+",");
            }
            writer.println(Arrays.toString(hiddenToOutputWeights).replace("[", "").replace("]", "")+",");
            writer.println(Arrays.toString(hiddenBiasWeight).replace("[", "").replace("]", "")+",");
            writer.println(outputBiasWeight);
            writer.close();
        } catch (IOException e) {
            System.out.println("Unable to size nn weights!");
        }
	}

	/* (non-Javadoc)
	 * @see CommonInterface#load(java.lang.String)
	 */
	@Override
	public void load(String argFileName) throws IOException {
        Scanner s = new Scanner(new File(argFileName));
        int numInputsFile = s.nextInt();
        int numHiddenFile = s.nextInt();
        s.nextLine();
        for(int i = 0; i < numHiddenFile; i++) {
            String [] row = s.nextLine().split(",");
            System.out.println(Arrays.toString(row));
            for (int j = 0; j < row.length; j++) {
                //System.out.println(s.next());
                inputsToHiddenWeights[i][j] = Float.parseFloat(row[j]);
            }
        }
        String [] row = s.nextLine().split(",");
        for(int i = 0; i < row.length; i++) {
            hiddenToOutputWeights[i] = Float.parseFloat(row[i]);
        }
        row = s.nextLine().split(",");
        for(int i = 0; i < row.length; i++) {
            hiddenBiasWeight[i] = Float.parseFloat(row[i]);
        }
        outputBiasWeight = s.nextFloat();
        this.numHidden = numHiddenFile;
        this.numInputs = numInputsFile;
        if (s.hasNext()) {
            System.out.println("File was malformed, expect weird net behavior!");
        }
        else {
            System.out.println("File read successful!");
        }
        rebuild();
	}

    /*
    Returns the result of the current activation function
     */
	public float activationFunction(float x) {
        switch (sigmoidType) {
            case BINARY:
                return binarySigmoid(x);
            case BIPOLAR:
                return bipolarSigmoid(x);
            default:
                System.out.println("Sigmoid returning 0!");
                return 0;
        }
    }

	/* (non-Javadoc)
	 * @see NeuralNetInterface#sigmoid(float)
	 */
	@Override
	public float binarySigmoid(float x) { return (float) (1.0f/(1.0f+Math.exp(-x))); }

	/* (non-Javadoc)
	 * @see NeuralNetInterface#customSigmoid(float)
	 * Does this still need to be here?
	 */
	@Override
	public float bipolarSigmoid(float x) { return (float) (2.0f/(1.0f+Math.exp(-x))-1.0f); }

	/*
	 * Calculate the gradient function for whichever sigmoid
	 */
	public float sigmoidDx(float x) {
        switch (sigmoidType) {
            case BINARY:
                return (float) (x*(1.0-x));
            case BIPOLAR:
                //return x*(1.0-x);
                return (float) (0.5*(1.0+x)*(1.0-x));
                //return 0.5*(2.0/(1.0+Math.exp(-x)))*(2.0-2.0/(1.0+Math.exp(-x)));
                //return 2.0*Math.exp(x)/Math.pow(Math.exp(x)+1.0,2.0);
            default:
                System.out.println("Sigmoid dx returning 0!");
                return 0;
        }
    }

	/* (non-Javadoc)
	 * @see NeuralNetInterface#initializeWeights()
	 * Init the hidden layer input weights and the weights for the output neuron
	 */
	@Override
	public void initializeWeights() {
        Random generator = new Random();
		inputsToHiddenWeights = new float[numHidden][numInputs];
		hiddenBiasWeight = new float[numHidden];
        for (int i = 0; i < hiddenBiasWeight.length; i++) {
            hiddenBiasWeight[i] = generator.nextFloat()*(upperBound - lowerBound) + lowerBound;
        }
        outputBiasWeight = (float) generator.doubles(1, lowerBound, upperBound).toArray()[0];
        for (int neuron = 0; neuron < inputsToHiddenWeights.length; neuron++) {
            for (int i = 0; i < inputsToHiddenWeights[neuron].length; i++) {
                inputsToHiddenWeights[neuron][i] = generator.nextFloat()*(upperBound - lowerBound) + lowerBound;
            }
            //inputsToHiddenWeights[neuron] = generator.doubles(numInputs, lowerBound, upperBound).toArray();
        }
        hiddenToOutputWeights = new float[numHidden];
        for (int i = 0; i < hiddenToOutputWeights.length; i++) {
            hiddenToOutputWeights[i] = generator.nextFloat()*(upperBound - lowerBound) + lowerBound;
        }
        //hiddenToOutputWeights = generator.doubles(numHidden, lowerBound, upperBound).toArray();
	}

	/* (non-Javadoc)
	 * @see NeuralNetInterface#zeroWeights()
	 */
	@Override
	public void zeroWeights() {
		// TODO Auto-generated method stub

	}

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public float [] getRangeValues() {
        return new float[2];
    }
}
