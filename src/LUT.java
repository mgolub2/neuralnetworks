import java.io.*;
import java.util.*;

/**
 * Created by mgolub2 on 11/9/16.
 */

public class LUT implements CommonInterface {

    private HashMap<String, Float> lut;
    private int numInputs;
    private int numAction;
    private static final Random floatGen = new Random();
    private float[] stateSpaceScaler;
    public boolean load;
    /*

     */
    public LUT(int numInputs, int numActions, int[] inputRanges, float[] scaler, boolean load, String filename) {
        //TODO fail if inputRanges.length is != numInputs
        System.out.println("Size is estimated to be: " + estimateSize(inputRanges, scaler));
        lut = new HashMap<String, Float>();
        stateSpaceScaler = scaler;
        this.load = load;
        this.numAction = numActions;
        this.numInputs = numInputs;
        if (load) {
            try {
                this.load(filename);
            } catch (IOException e) {
                System.out.println("Unable to load :" + filename + "\nDefaulting to blank LUT.");
            }
        }
    }

    public int getSize(){
        return lut.size();
    }

    private int estimateSize(int [] inputRanges, float [] scaler) {
        float size = 1.0f;
        for(int i = 0; i < inputRanges.length; i++) {
            size *= 1+inputRanges[i]/scaler[i];
        }
        System.out.println(size);
        return (int) Math.round(size);
    }

    @Override
    /*
    Learn a new value, vased on an array key
     */
    public float train(float[] X, float argValue) {
        lut.put(vectorConvert(X), argValue);
        //System.out.println("Lut size: "+ lut.size());
        return argValue;
    }

    /*
    Learn a new value, based on a string key.
     */
    public float train(String X, float argValue) {
        lut.put(X, argValue);
        return argValue;
    }

    /*
    Return the output for a given vector x.
     */
    @Override
    public float outputFor(float[] X) {
        //return lut.get(vectorConvert(X));
        String key = vectorConvert(X);
        //System.out.println(key);
        if (lut.containsKey(key)) {
            return lut.get(key);
        }
        else {
            return train(key,  floatGen.nextFloat()); //lazily populate lut.
        }
    }

    /*
    Converts an int vector into a scaled string vector
     */
    //private stri

    /*
    Converts a float vector into a a scaled string for state space reduction.
     */
    public String vectorConvert(float [] X) {
        int [] vector = new int[X.length];
        for (int i =0; i < vector.length; i++) {
            //sort of a gross hack, we need to scale the field part of the inputs only.
            if (i < stateSpaceScaler.length) {
                vector[i] = (int) Math.round(X[i]/stateSpaceScaler[i]);
            }
            else {
                vector[i] = (int) Math.round(X[i]);
            }
        }
        return toConcatString(vector);
    }

    /*
    Convert a string key back to it's vector representation, undoing scaling (much fancy)

    will blow up if you give it a vector longer than the state space scaler YOLO
     */
    public float [] vectorCreate(String X) {
        //System.out.println(X);
        String [] splitStr = X.split(",");
        float [] vector = new float[splitStr.length];
        float min = 10;
        float max = -10;
        for(int i = 0; i < splitStr.length; i++) {
            if(i < stateSpaceScaler.length) {
                vector[i] = Float.parseFloat(splitStr[i]);
                if (vector[i] < min) {
                    min = vector[i];
                }
                if (vector[i] > max) {
                    max = vector[i];
                }
            }
            else {
                float action =  Float.parseFloat(splitStr[i]);
                if (action > .5) {
                    vector[i] = 1;
                }
                else {
                    vector[i] = -1;
                }
            }
        }
        return vector;
        //System.out.println(min);
        //System.out.println
        //return normalize(vector, min, max);
    }

    private float [] normalize(float [] nums, float min, float max) {
        for (int i =0; i < nums.length; i++) {
            nums[i] = (nums[i]-min)/(max-min);
        }
        //System.out.println(Arrays.toString(nums));
        return nums;
    }

    public static float normalize(float num, float min, float max) {
        return (num-min)/(max-min);
    }

    public float[] getRangeValues() {
        return new float[]{Collections.min(lut.values()), Collections.max(lut.values())}; //re'll fancy
    }

    public DataDump dumpKeyValues() {
        float range [] = getRangeValues();
        float [][] stateData = new float[lut.size()][numAction+numInputs];
        float [] expectedValue = new float[lut.size()];
        int i = 0;
        for (HashMap.Entry<String, Float> cursor : lut.entrySet()) {
            stateData[i] = vectorCreate(cursor.getKey());
            expectedValue[i] = normalize(cursor.getValue(), range[0], range[1]);
            i++;
        }
        return new DataDump(stateData, expectedValue);
    }

    //public float [] dumpValues() {
    //    return lut.values();
    //}

    /*
    Create a minimal concatentation of an array
     */
    private String toConcatString(int [] arr) {
        StringBuilder str = new StringBuilder();
        for (long anArr : arr) {
            str.append(anArr);
            str.append(',');
        }
        return str.toString();
    }

    @Override
    public void load(String argFileName) throws IOException {
        try {
            FileInputStream fileInputStream = new FileInputStream(argFileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            lut = (HashMap <String, Float>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Failed loading file: " + argFileName);
        }
    }

    @Override
    public void save(String argFile) {
        try {
            FileOutputStream fileStream = new FileOutputStream(argFile);
            ObjectOutputStream lutOut = new ObjectOutputStream(fileStream);
            lutOut.writeObject(lut);
            lutOut.close();
        } catch (IOException e) {
            System.out.println("Failed to save file: " +argFile);
        }
    }
}
