import java.util.Arrays;

/**
 * Created by mgolub2 on 12/1/16.
 */
public class OfflineNN {

    private static int numStates = 6;
    private static int numActions = 4;

    public static void main(String [] args) {
//        LUT trainingSet = new LUT(numStates, numActions,
//                new int[]{8,360,8,360, 600, 600}, new float[]{4,9,4,9, 300, 300},
//                true, "standard64_2.lut");
//        float [] range = trainingSet.getRangeValues();
//        System.out.println(Arrays.toString(range));
        NeuralNet offlineLUTTraining = new NeuralNet(11,
                8, 0.01f, 0.9f,
                -5f, 5f, NeuralNet.ActivationType.BIPOLAR,
                false, true, new float[]{4,9,4,9, 300, 300});
        //DataDump data = trainingSet.dumpKeyValues();
        //offlineLUTTraining.run(data.stateData, data.valueData);
        System.out.println(offlineLUTTraining.outputFor(new float[]{0,0,0,0,0,0,0,0,0,0,0}));
    }
}
