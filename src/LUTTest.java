import static org.junit.Assert.*;

/**
 * Created by mgolub2 on 11/16/16.
 */
public class LUTTest {

    float [][] testStates = new float[][]{
            {25, 8, 800, 800, 360, 8, 100},
            {24, 7, 300, 500, 91, 6, 80},
            {74, 1, 0, 0, -360, 6, 80},
            {76, 2, 1, 1, -350, 5, 79}
    };

    LUT test = new LUT(7, 5, new int[]{100,8,800,800,360,8,100}, new float[]{25,2,400,400,90,2,25},false, "brain.lut");
    String expectedStrings[] = new String[]{
            "1422444",
            "1411133",
            "3100-433",
            "3100-433"
    };

    @org.junit.Test
    public void train() throws Exception {
        for(int i = 0; i < expectedStrings.length; i++ ) {
            test.train(testStates[i], i);
        }
        assertEquals(0, test.outputFor(testStates[0]), .00001);
        assertEquals(1, test.outputFor(testStates[1]), .00001);
        assertEquals(3, test.outputFor(testStates[2]), .00001);
        assertEquals(3, test.outputFor(testStates[3]), .00001);
    }

    @org.junit.Test
    public void outputFor() throws Exception {
        assertEquals(test.outputFor(testStates[2]), test.outputFor(testStates[3]), .00001);
    }

    @org.junit.Test
    public void vectorConvert() throws Exception {
        for (int i = 0; i < testStates.length; i++) {
            assertEquals(expectedStrings[i], test.vectorConvert(testStates[i]));
        }
    }

}