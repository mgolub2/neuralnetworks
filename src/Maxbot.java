import robocode.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
/**
 * Created by mgolub2 on 11/9/16.
 */
public class Maxbot extends AdvancedRobot {

    int moveDirection=1;//which way to move
    private ScannedRobotEvent lastEvent;
    //possible actions TODO make this dynamically expandable
    private static final float[] FIRE = new float[]{1,-1,-1,-1,-1};
    //private static final float[] DOWN = new float[]{-1,1,-1,-1,-1};
    private static final float[] UP = new float[]{-1,-1,1,-1,-1};
    private static final float[] LEFT = new float[]{-1,-1,-1,1,-1};
    private static final float[] RIGHT = new float[]{-1,-1,-1,-1,1};
    //private static final float[] NONE = new float[]{0,0,0,0,0};
    private static final float[][] ACTIONS = new float[][]{FIRE, UP, LEFT, RIGHT};
    private static final float PIXELS = 30;
    //alpha parameter for q learning
    private static final float alpa = .05f;
    private static final float gamma = .2f;
    float latVel = 0;
    private static float R = 0;
    //Performance data
    private static int numRounds = 0;
    private static int numWins = 0;
    private static XYSeries winRecord = new XYSeries("Wins per 100 rounds");
    private static XYSeries percentHit = new XYSeries("Percent hit per 100 rounds");
    private static XYSeries LUTSize = new XYSeries("LUT Size");
    private static XYSeries Ram = new XYSeries("Rams per 100");
    private static float numMissed = 0;
    private static float numHit = 0;
    private static int onRam = 0;
    private static boolean battle = true;
    private static boolean policy = false;
    private static boolean terminalOnly = false;
    private static boolean learn = true;
    private static float exploration = 0.0f;
    private static Random exploreGen = new Random();
    int explores = 0;
    private static boolean nn = true;
    private static final CommonInterface brain = init_brain(nn);
    private static float [] specialQA = new float[]{2, 40, 1, 80, 1, 1, 1, -1, -1, -1, -1};
    private static XYSeries QSA = new XYSeries("Q value for {2, 40, 1, 80, 1, 1, 1, 0, 0, 0, 0} over Time");
    private static float oldQA = 0;

    public static CommonInterface init_brain(boolean nn) {
        if (nn) {
            return new NeuralNet(11,
                    8, 0.01f, 0.9f,
                    -.5f, .5f, NeuralNet.ActivationType.BIPOLAR,
                    false, true, new float[]{4, 9, 4, 9, 300, 300});
        } else {
            return new LUT(6, 4, new int[]{8, 360, 8, 360, 600, 600},
                    new float[]{4, 9, 4, 9, 300, 300}, false, "standard64.lut");
        }
    }

    public void run() {
        setAdjustRadarForRobotTurn(true);//keep the radar still while we turn
        setBodyColor(new Color(240, 128, 50));
        setGunColor(new Color(50, 0, 100));
        setRadarColor(new Color(255, 200, 0));
        setScanColor(Color.green);
        setBulletColor(Color.yellow);
        //turnRadarRight(360);
        //setTurnRadarLeft(Double.POSITIVE_INFINITY);
        float [] currentState = generateStateVector();
        int currentAction = maxAction(currentState);
        while(battle) {
            setTurnRadarLeft(90);
            preformAction(currentAction);
            //System.out.println(R);
            if (learn) {
                currentState = updateLUT(R, currentState, currentAction); //updates the lut and generates new state
                boolean greedy = exploreGen.nextFloat() >= exploration;
                if(greedy) {
                    currentAction = maxAction(currentState);
                }
                else {
                    explores ++;
                    currentAction = exploreGen.nextInt(ACTIONS.length);
                }
                R = 0;
            }
            else {
                currentState = generateStateVector();
                currentAction = maxAction(currentState);
            }
        }
    }

    private float [] updateLUT(float reward, float [] currentState, int currentAction) {
        float [] nextState = generateStateVector();
        if (learn) {
            float[] key = concatFloat(currentState, ACTIONS[currentAction]);
            float oldValue = brain.outputFor(key);
            float newValue;
            if (policy) {
                newValue = brain.outputFor(concatFloat(nextState, ACTIONS[currentAction]));
            } else {
                int nextAction = maxAction(nextState);
                newValue = brain.outputFor(concatFloat(nextState, ACTIONS[nextAction]));
            }
            //if (nn) {
            //    brain.train(key,  LUT.normalize(oldValue + alpa * (reward + gamma * newValue - oldValue), -10, 10));
            //}
            //else {
                brain.train(key, oldValue + alpa * (reward + gamma * newValue - oldValue));
            //}
        }
        return nextState;
    }

    /*
        From http://stackoverflow.com/questions/80476/how-can-i-concatenate-two-arrays-in-java
         */
    private float[] concatFloat(float[] a, float[] b) {
        int aLen = a.length;
        int bLen = b.length;
        float[] c= new float[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    /*
    Get the current status, directly. This will be "skewed" slightly, since the it is not based on a single round. 
     */
    private float[] generateStateVector() {
        try {
            //return new float[]{Math.abs(getVelocity()), Math.abs(getX()), Math.abs(getY()),
            //        Math.abs(getGunHeading()), Math.abs(latVel)};
            return new float[]{(float)Math.abs(getVelocity()), (float)Math.abs(getGunHeading()), Math.abs(latVel), (float) Math.abs(lastEvent.getBearing()),
                    (float)getX(), (float)getY()};
        } catch (NullPointerException exception) { //0 out fields in null lastEvent
            //return new float[]{Math.abs(getVelocity()), Math.abs(getX()), Math.abs(getY()),
            //        Math.abs(getGunHeading()), 0}; //fake high value
            return new float[]{(float)Math.abs(getVelocity()), (float) Math.abs(getGunHeading()), 8, 360, (float) getX(), (float) getY()}; //fake high value
        }
    }

    /*
    Preform a selected action

    TODO - enable more than one action, make numbers clearer?
     */
    private void preformAction(int action) {
        switch (action) { //does this work??
            case 0: //fire
                setFire(Rules.MAX_BULLET_POWER);
                break;
            case 1: //up, ahead
                setAhead(PIXELS*moveDirection);
                break;
            case 2: //down
                setAhead(PIXELS*-moveDirection);
                break;
            case 3: //left
                setTurnLeft(90);
                setAhead(PIXELS*moveDirection);
                break;
            case 4: //right
                setTurnRight(90);
                setAhead(PIXELS*moveDirection);
                break;
        }
        execute();
    }

    /*
    Based on the possible actions our robot can preform, select the one with the highest probability.

    TODO return a neater respresentation of an action then an entire array...
     */
    private int maxAction(float [] stateVector) {
        float maxProb = 0;
        int selectedAction = 1; //None
        for (int i = 0; i < ACTIONS.length; i++) {
            float currentProb = brain.outputFor(concatFloat(stateVector, ACTIONS[i]));
            //System.out.println("Max action is: " + currentProb);
            if (currentProb > maxProb) {
                maxProb = currentProb;
                selectedAction = i;
            }
        }
        return selectedAction; //TODO possible return everything to reduce lookups.
    }

    /**
     * onScannedRobot:  Here's the good stuff
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        lastEvent = e;
        double absBearing= (e.getBearingRadians()+getHeadingRadians());//enemies absolute bearing
        this.latVel= (float) (e.getVelocity() * Math.sin(e.getHeadingRadians() -absBearing));//enemies later velocity
        double gunTurnAmt;//amount to turn our gun
        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());//lock on the radar
        gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/15);//amount to turn our gun, lead just a little bit
        setTurnGunRightRadians(gunTurnAmt);
    }

    private void bookkeeping() {
        //System.out.println(getNumRounds());
        numRounds++;
        float currentQA  = brain.outputFor(specialQA);
        QSA.add(numRounds, currentQA-oldQA);
        oldQA = currentQA;
        if (numRounds % 250 == 0) {
            winRecord.add(numRounds, numWins/250.0*100.0);
            percentHit.add(numRounds, numHit/numMissed*100);
            LUTSize.add(numRounds, brain.getSize());
            Ram.add(numRounds, numWins/numMissed*100);
            numMissed = 0;
            numHit = 0;
            numWins = 0;
            onRam = 0;
        }
    }

    /**
     * onWin:  Do a victory dance
     */
    public void onWin(WinEvent e) {
        R = 100;
        numWins++;
        bookkeeping();
    }

    @Override
    public void onDeath(DeathEvent event) {
        R = -100;
        bookkeeping();
    }

    @Override
    public void onBulletHit(BulletHitEvent event) {
        numHit ++;
        numMissed++;//using as total
        //if (!terminalOnly) {
            R = 50;
        //}
    }

//    @Override
//    public void onHitByBullet(HitByBulletEvent event) {
//        if (!terminalOnly) {R += -150;}
//    }

    @Override
    public void onBulletMissed(BulletMissedEvent event) {
        numMissed ++;
        //if (!terminalOnly) {R += -30;}
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        //R += -10;
        moveDirection *= -1;
        setAhead(PIXELS*10);
    }

    @Override
    public void onHitRobot(HitRobotEvent event) {
        onRam++;
    }

    @Override
    public void onBattleEnded(BattleEndedEvent event) {
        battle = false;
        String nameStart = "terminal-"+Boolean.toString(terminalOnly)+"-policy-"
                +Boolean.toString(policy)+"-explore-"+Float.toString(exploration);
        outputGraph(winRecord, "Win_Record-"+nameStart);
        outputGraph(percentHit, "Percent_Hit-"+nameStart);
        outputGraph(LUTSize, "LUTSIZE-"+nameStart);
        outputGraph(Ram, "Rams-"+nameStart);
        outputGraph(QSA, "Q value for {2, 40, 1, 80, 1, 1, 1, 0, 0, 0, 0} over Time");
        if(learn) {
            System.out.println("Saving brain");
            brain.save("brain.lut");
        }
        try{
            PrintWriter writer = new PrintWriter("stats.txt", "UTF-8");
            writer.println(Arrays.toString(brain.getRangeValues()));
            writer.println(brain.getSize());
            writer.println(explores);
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }

    @Override
    /*
    Save the current lut
     */
    public void onKeyTyped(KeyEvent e) {
        if (e.getKeyChar() == 's') {
            brain.save("brain.lut");
        }
    }

    private void outputGraph(XYSeries data, String GraphName) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(data);
        JFreeChart chart = ChartFactory.createXYLineChart(
                GraphName, // chart title
                "Rounds", // axis label
                "", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                false, // include legend
                true, // tooltips
                false // urls
        );
        try {
            DateFormat dateFormat = new SimpleDateFormat("-HH-mm-ss");
            Date date = new Date();
            ChartUtilities.saveChartAsJPEG(new File(GraphName.replace(" ", "-")+dateFormat.format(date)+".jpg"), chart, 640, 480);
        }
        catch (IOException e) {
            System.out.println("Unable to save error graph.");
        }
    }
}
